var allUSCrimes = ['VC','MNM','RLD','R','AA','PC','B','LT','MVT'];
var allUKCrimes = ['V','SO','B','VO','T','R','S','F'];
var crimeAvgUS = [[],[],[],[],[],[],[],[],[]];
var crimeAvgUK = [[],[],[],[],[],[],[],[]];
var colors = ["#ffb2a1","#ff907b","#f5634e","#ea3a2d","#e41a1c",
                "#c6d5e9","#a2bbdb","#749dca","#4c87be","#377eb8"];
var prevArr = [];

/*
 * computes the average value of an array
 * and returns the value
 */
function getAverage(arr){
    for(var i=0;i<arr.length;i++){
        var sum = arr[i].reduce(function (currentSum,n){
            return currentSum+n;
        });
        arr[i]=sum/arr[i].length;
    }
    return arr;
}

/*
 * sorts an array by decending form
 * after the array is sorted it finds the original position
 * of the value so that its name can be found and an
 * object is created with a name and value for every entry in the array
 * an array with all created objects is returned
 */
function computeMostCommon(names,avgArr){
    var mostCommonArr = [];
    var sorted = [].concat(avgArr);
    sorted = sorted.sort(function(a, b){return b - a});
    for(var i=0;i<sorted.length;i++){
        var idx = avgArr.indexOf(sorted[i]);
        var o = {name:names[idx],value:sorted[i]};
        mostCommonArr.push(o);
    }
    return mostCommonArr;
}

/*
 * draws the bubble chart using D3
 * it takes the data that needs to be drawn, the name of the
 * function that needs to be used to get the names of crimes,
 * the id od the div in which the bubble graph would be draw,
 * the normally sorted array(decending) and the start of the data
 * if it was a combined array
 */
function createBubbles(data,nameFunction,id,sorted,start){
    data = data.slice(0,5);
    var diameter = 300;

    var bubble = d3.layout.pack()
        .sort(null)
        .size([diameter, diameter])
        .padding(1.5);

    d3.select(id)
        .append("h2")
        .text(function (){
            if(id=="#BubbleUS"){
                return "USA";
            }else{
                return "United Kingdom";
            }
    });

    var svg = d3.select(id)
        .append("svg")
        .attr("width", diameter)
        .attr("height", diameter)
        .attr("class", "bubble");

    var nodes = bubble.nodes({children:data}).filter(function(d) { return !d.children; });
    var bubbles = svg.append("g")
        .attr("transform", "translate(0,0)")
        .selectAll(".bubble")
        .data(nodes)
        .enter();

    bubbles.append("circle")
        .attr("r", function(d){ return d.r; })
        .attr("cx", function(d){ return d.x; })
        .attr("cy", function(d){ return d.y; })
        .style("fill", function(d,i){
            //coloring of each bubble follow the sorter order
            //rather than the order of the data
            var colorSelected = false;
            for(var j=0;j<sorted.length;j++){
                if(i!==j && d===sorted[j]){
                    colorSelected = true;
                    return colors[j];
                }
            }
            if(!colorSelected){
                return colors[i+start];
            }});

    bubbles.append("text")
        .attr("x", function(d){ return d.x; })
        .attr("y", function(d){ return d.y; })
        .attr("text-anchor", "middle")
        .text(function(d){ return nameFunction(d["name"]); })
        .style({"font-size": "12px"});
}

/*
 * gets the name of an object and if it
 * exists the full name is returned
 * This function is for US crimes only
 */
function getActualNameUS(s){
    switch(s){
        case "VC": return "Violent Crime";
        case "MNM": return "Murder and nonnegligent manslaughter";
        case "RLD": return "Rape";
        case "R": return "Robbery";
        case "AA": return "Aggravated assault";
        case "PC": return "Property crime";
        case "B": return "Burglary";
        case "LT": return "Larceny-theft";
        case "MVT": return "Motor vehicle theft";
    }
}

/*
 * gets the name of an object and if it
 * exists the full name is returned
 * This function is for UK crimes only
 */
function getActualNameUK(s){
    switch(s){
        case "V": return "Violence";
        case "SO": return "Sexual Offences";
        case "R": return "Robbery";
        case "S": return "Shoplifting";
        case "F": return "Firearms";
        case "B": return "Burglary";
        case "T": return "Theft";
        case "VO": return "Vehicle offences";
    }
}

/*
 * loads the data needed for animated Bubbles
 * deletes previous visualization
 * call the creation of of the timeline
 * and call the method for the visualization of animated bubbles
 */
function animateBubbles(){
    deletePrevious();
    var size = 85;
    smallMultipleBubles(size);
    var crimeAvgUS = [[],[],[],[],[],[],[],[],[]];
    var crimeAvgUK = [[],[],[],[],[],[],[],[]];
    d3.csv("USCrimeData.csv", function(error, data) {
        data = data.slice(5);
        for(var i=0;i<data.length;i++){
            crimeAvgUS[0].push(+data[i].VCR);
            crimeAvgUS[1].push(+data[i].MNMR);
            crimeAvgUS[2].push(+data[i].RLDR);
            crimeAvgUS[3].push(+data[i].RR);
            crimeAvgUS[4].push(+data[i].AAR);
            crimeAvgUS[5].push(+data[i].PCR);
            crimeAvgUS[6].push(+data[i].BR);
            crimeAvgUS[7].push(+data[i].LTR);
            crimeAvgUS[8].push(+data[i].MVTR);
        }
        d3.csv("UKCrimeData.csv", function(error, data) {
            for(var i=0;i<data.length;i++){
                crimeAvgUK[0].push(+data[i].V);
                crimeAvgUK[1].push(+data[i].SO);
                crimeAvgUK[2].push(+data[i].B);
                crimeAvgUK[3].push(+data[i].VO);
                crimeAvgUK[4].push(+data[i].T);
                crimeAvgUK[5].push(+data[i].R);
                crimeAvgUK[6].push(+data[i].S);
                crimeAvgUK[7].push(+data[i].F);
            }
            animateB(crimeAvgUS,crimeAvgUK,0);
        });
    });
}

/*
 * this method extracts the data for a specific year and
 * calls the visualization for that year
 * then a timeout is set before that method is called again
 * The timeout simulates the movement of the bubbles while in
 * fact they are redrawn every single time
 * this method would be axecuted only 15 times before it stops
 * because the data is between 2002 and 2016
 */
function animateB(USData,UKData,count){
    USDCurrent = [];
    UKDCurrent = [];
    if(count>=0&&count<UKData[0].length){
        deletePreviousWOSM();
        for(var i=0;i<UKData.length;i++){
            UKDCurrent.push(UKData[i][count]);
        }
        for(var i=0;i<USData.length;i++){
            USDCurrent.push(USData[i][count]);
        }
        d3.select("#BubbleUS")
        .append("h1")
        .text(count+2002);
        var mostArrUS = computeMostCommon(allUSCrimes,USDCurrent).slice(0,5);
        var mostArrUK = computeMostCommon(allUKCrimes,UKDCurrent).slice(0,5);
        var combinedArr = mostArrUK.concat(mostArrUS);
        combinedArr = sortByPrevPosition(prevArr,combinedArr);
        prevArr = [].concat(combinedArr);
        var mostArrUK1 = combinedArr.slice(0,5);
        var mostArrUS1 = combinedArr.slice(5);
        createBubbles(mostArrUS1,getActualNameUS,"#BubbleUS",mostArrUS,5);
        createBubbles(mostArrUK1,getActualNameUK,"#BubbleUK",mostArrUK,0);
        count++;
        setTimeout(animateB, 1000,USData,UKData,count);
    }
}

/*
 * deletes all elements that are in divs
 * in this case all visualizations
 */
function deletePrevious(){
    var allDivs = document.getElementsByTagName('div');
    for(var j=0;j<allDivs.length;j++){
        while (allDivs[j].firstChild) {
            allDivs[j].removeChild(allDivs[j].firstChild);
        }
    }
}

/*
 * this method deletes all elements that are in a div
 * with an exception of the elements that are used for small
 * mutiples in the animation
 * This is used so that there is no need to load the same information
 * and then do the same visualization for each redraw of the animated
 * Bubble graph
 */
function deletePreviousWOSM(){
    var allDivs = document.getElementsByTagName('div');
    var allSmallMultiples = ["SM0","SM1","SM2","SM3","SM4","SM5","SM6","SM7",
                    "SM8","SM9","SM10","SM11","SM12","SM13","SM14","smallMultiples"];
    for(var j=0;j<allDivs.length;j++){
        if(allSmallMultiples.indexOf(allDivs[j].id)<0){
            console.log(allDivs[j].id);
            while (allDivs[j].firstChild) {
                allDivs[j].removeChild(allDivs[j].firstChild);
            }
        }
    }
}

/*
 * loads all US and UK data and does all computations needed
 * in order for them to be visualized in static way
 * In the static visualization the value that is presented is
 * the average of all years
 * then the visualization for each contry and their combined one
 * is invoked
 */
function staticBubbles(){
    deletePrevious();
    var crimeAvgUS = [[],[],[],[],[],[],[],[],[]];
    var crimeAvgUK = [[],[],[],[],[],[],[],[]];
    d3.csv("USCrimeData.csv", function(error, data) {
        data = data.slice(5);
        for(var i=0;i<data.length;i++){
            crimeAvgUS[0].push(+data[i].VCR);
            crimeAvgUS[1].push(+data[i].MNMR);
            crimeAvgUS[2].push(+data[i].RLDR);
            crimeAvgUS[3].push(+data[i].RR);
            crimeAvgUS[4].push(+data[i].AAR);
            crimeAvgUS[5].push(+data[i].PCR);
            crimeAvgUS[6].push(+data[i].BR);
            crimeAvgUS[7].push(+data[i].LTR);
            crimeAvgUS[8].push(+data[i].MVTR);
        }
        d3.csv("UKCrimeData.csv", function(error, data) {
            for(var i=0;i<data.length;i++){
                crimeAvgUK[0].push(+data[i].V);
                crimeAvgUK[1].push(+data[i].SO);
                crimeAvgUK[2].push(+data[i].B);
                crimeAvgUK[3].push(+data[i].VO);
                crimeAvgUK[4].push(+data[i].T);
                crimeAvgUK[5].push(+data[i].R);
                crimeAvgUK[6].push(+data[i].S);
                crimeAvgUK[7].push(+data[i].F);
            }
            crimeAvgUK = getAverage(crimeAvgUK);
            var mostArrUK = computeMostCommon(allUKCrimes,crimeAvgUK);
            mostArrUK = mostArrUK.slice(0,5);
            crimeAvgUS = getAverage(crimeAvgUS);
            var mostArrUS = computeMostCommon(allUSCrimes,crimeAvgUS);
            mostArrUS = mostArrUS.slice(0,5);
            createBubbles(mostArrUS,getActualNameUS,"#BubbleUS",mostArrUS,5);
            createBubbles(mostArrUK,getActualNameUK,"#BubbleUK",mostArrUK,0);
            var combinedArr = mostArrUK.concat(mostArrUS);
            combineBubble(combinedArr,300,"combined",combinedArr);
        });
    });
}

/*
 * creates a bubble graph that uses the data for both contries
 * and allows the user to compare the data because then they are
 * on the same scale
 */
function combineBubble(dataArr,diameter,divId,sorted){
    var bubble = d3.layout.pack()
        .sort(null)
        .size([diameter, diameter])
        .padding(1.5);

    var svg = d3.select("#" + divId)
        .append("svg")
        .attr("width", diameter)
        .attr("height", diameter)
        .attr("class", "bubble");

    var nodes = bubble.nodes({children:dataArr}).filter(function(d) { return !d.children; });
    var bubbles = svg.append("g")
        .attr("transform", "translate(0,0)")
        .selectAll(".bubble")
        .data(nodes)
        .enter();

    bubbles.append("circle")
        .attr("r", function(d){ return d.r; })
        .attr("cx", function(d){ return d.x; })
        .attr("cy", function(d){ return d.y; })
        .style("fill", function(d,i){
            var colorSelected = false;
            for(var j=0;j<sorted.length;j++){
                if(i!==j && d===sorted[j]){
                    colorSelected = true;
                    return colors[j];
                }
            }
            if(!colorSelected){
                return colors[i];
            }
        });
        
    bubbles.append("text")
        .attr("x", function(d){ return d.x; })
        .attr("y", function(d){ return d.y+5; })
        .attr("text-anchor", "middle")
        .text(function(d){return d["name"];})
        .style({"font-size": "10px"});
}

/*
 * loads the UK and US data and creates a combined graph for each
 * year where the data for the two countries can be compared
 * it takes size as an argument to that it can be used for
 * bothe the timeline and the small multiples visualization
 */
function smallMultipleBubles(size){
    var crimeAvgUS = [[],[],[],[],[],[],[],[],[]];
    var crimeAvgUK = [[],[],[],[],[],[],[],[]];
    prevArr = [];
    d3.csv("USCrimeData.csv", function(error, data) {
        data = data.slice(5);
        for(var i=0;i<data.length;i++){
            crimeAvgUS[0].push(+data[i].VCR);
            crimeAvgUS[1].push(+data[i].MNMR);
            crimeAvgUS[2].push(+data[i].RLDR);
            crimeAvgUS[3].push(+data[i].RR);
            crimeAvgUS[4].push(+data[i].AAR);
            crimeAvgUS[5].push(+data[i].PCR);
            crimeAvgUS[6].push(+data[i].BR);
            crimeAvgUS[7].push(+data[i].LTR);
            crimeAvgUS[8].push(+data[i].MVTR);
        }
        d3.csv("UKCrimeData.csv", function(error, data) {
            for(var i=0;i<data.length;i++){
                crimeAvgUK[0].push(+data[i].V);
                crimeAvgUK[1].push(+data[i].SO);
                crimeAvgUK[2].push(+data[i].B);
                crimeAvgUK[3].push(+data[i].VO);
                crimeAvgUK[4].push(+data[i].T);
                crimeAvgUK[5].push(+data[i].R);
                crimeAvgUK[6].push(+data[i].S);
                crimeAvgUK[7].push(+data[i].F);
            }
            for(var j=0;j<15;j++){
                var USDCurrent = [];
                var UKDCurrent = [];
    
                for(var i=0;i<crimeAvgUK.length;i++){
                    UKDCurrent.push(crimeAvgUK[i][j]);
                }
                for(var i=0;i<crimeAvgUS.length;i++){
                    USDCurrent.push(crimeAvgUS[i][j]);
                }
                var id = "SM" + j;
                d3.select("#smallMultiples")
                        .append("div")
                        .attr("id",(id))
                        .style({"display":"inline-block"});
                d3.select("#" + id)
                        .append("h6")
                         .text(j+2002);
                var mostArrUS = computeMostCommon(allUSCrimes,USDCurrent);
                var mostArrUK = computeMostCommon(allUKCrimes,UKDCurrent);
                mostArrUS = mostArrUS.slice(0,5);
                mostArrUK = mostArrUK.slice(0,5);
                var combinedArr = mostArrUK.concat(mostArrUS);
                var sortedArr = [].concat(combinedArr);
                combinedArr = sortByPrevPosition(prevArr,combinedArr);
                prevArr = [].concat(combinedArr);
                combineBubble(combinedArr,size,id,sortedArr);
    }
        });
    });
}

/*
 * this method sort an array using the previous positions
 * of the elements
 * This sorting allow the elements not to change their possition
 * during the animation visualization
 */
function sortByPrevPosition(prevArr,curentArr){
    var newPossitions = new Array(10).fill(0);
    for(var i=0;i<curentArr.length;i++){
        var found = false;
        for(var j=0;j<prevArr.length;j++){
            if(curentArr[i].name===prevArr[j].name){
                if((i<5&&j<5)||(i>=5 && j>=5)){
                    newPossitions[j] = curentArr[i];
                    found = true;
                }
            }
        }
        if(!found){
            newPossitions[i] = curentArr[i];
        }
    }
    return newPossitions;
}

/*
 * this method deleted previous visualizations
 * and calls the small multiple visualization method
 */
function smBubbles(){
    deletePrevious();
    var size = 250;
    smallMultipleBubles(size);
}