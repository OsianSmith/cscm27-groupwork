import pandas as pd
import glob
import  os
os.system("rm  out.csv")
path =r'/Users/osiansmith/Downloads/0906a057c1e1b3cd84741a9a37ac05e3b1a32a57/2018-09' # use your path
allFiles = glob.glob(path + "/*.csv")

list_ = []

for file_ in allFiles:
    df = pd.read_csv(file_,index_col=None, header=0)
    list_.append(df)


frame = pd.concat(list_, axis = 0, ignore_index = True)

frame = frame.drop(["Crime ID","Reported by","Location","Latitude","Longitude","LSOA code","LSOA name","Last outcome category","Context"], axis=1)

#this maps the code in the constabulary
constabulary = {}
constabularyCode = 0
for i in range(len(frame)):
    if frame.iloc[i,1] in constabulary:
        print("No new constabulary")
    else:
        constabulary[frame.iloc[i,1]] = constabularyCode
        constabularyCode = constabularyCode + 1


frame["Falls within"] =  frame["Falls within"].map(constabulary)


crime = {}
crimeCode = 0
for i in range(len(frame)):
    if frame.iloc[i,2] in crime:
        print("No new Crime")
    else:
        crime[frame.iloc[i,2]] = crimeCode
        crimeCode = crimeCode + 1


frame["Crime type"] = frame["Crime type"].map(crime)


print("constabulary")
print(constabulary)

print("\n\n\n\n\n")
print("crime")
print(crime)

print("\n\n\n\n\n")

print("***")
print(frame['Falls within'])
frame.to_csv("out.csv")

