import pandas as pd
import glob
import  os

print("Setting path")
path =r'/Users/osiansmith/Desktop/Data' # use your path
allFiles = glob.glob(path + "/*.csv")

list_ = []

for file_ in allFiles:
    df = pd.read_csv(file_)
    list_.append(df)

frame = pd.concat(list_, axis = 0, ignore_index = True, sort= False)
print("actually doing constab")
constabDic = {'Avon and Somerset Constabulary': 0, 'Devon & Cornwall Police': 1, 'Essex Police': 2, 'South Wales Police': 3, 'Greater Manchester Police': 4, 'Cleveland Police': 5, 'Warwickshire Police': 6, 'British Transport Police': 7, 'North Wales Police': 8, 'Bedfordshire Police': 9, 'Gloucestershire Constabulary': 10, 'West Yorkshire Police': 11, 'Kent Police': 12, 'Merseyside Police': 13, 'Dorset Police': 14, 'Metropolitan Police Service': 15, 'Lincolnshire Police': 16, 'Lancashire Constabulary': 17, 'Leicestershire Police': 18, 'Staffordshire Police': 19, 'West Mercia Police': 20, 'Police Service of Northern Ireland': 21, 'City of London Police': 22, 'Wiltshire Police': 23, 'Northamptonshire Police': 24, 'Derbyshire Constabulary': 25, 'Humberside Police': 26, 'Durham Constabulary': 27, 'South Yorkshire Police': 28, 'Cambridgeshire Constabulary': 29, 'Hertfordshire Constabulary': 30, 'Norfolk Constabulary': 31, 'North Yorkshire Police': 32, 'Surrey Police': 33, 'Hampshire Constabulary': 34, 'Sussex Police': 35, 'Suffolk Constabulary': 36, 'Gwent Police': 37, 'Cumbria Constabulary': 38, 'Northumbria Police': 39, 'Dyfed-Powys Police': 40, 'Thames Valley Police': 41, 'Nottinghamshire Police': 42, 'West Midlands Police': 43, 'Cheshire Constabulary': 44}
frame["Falls within num"] =  frame["Falls within"].map(constabDic)

crimeDic = {'Other theft': 0, 'Anti-social behaviour': 1, 'Burglary': 2, 'Criminal damage and arson': 3, 'Possession of weapons': 4, 'Public order': 5, 'Vehicle crime': 6, 'Violence and sexual offences': 7, 'Drugs': 8, 'Robbery': 9, 'Bicycle theft': 10, 'Other crime': 11, 'Shoplifting': 12, 'Theft from the person': 13}

frame["crimeTypeNum"] = frame["Crime type"].map(crimeDic)
print("saving")
frame.to_csv("out1.csv")


