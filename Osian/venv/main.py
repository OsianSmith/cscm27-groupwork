import pandas as pd
import  os
import glob
import time

print("UK Crime data parser by Osian  - on initla runs or after unsuccessful runs we will hit a error - ignore until I say otherwise ")
os.system("del  out.csv")
print("Clear of any issues - any errors now forward will be bad ")
path =r'D:\VAData\2018-09'# use your path
allFiles = glob.glob(path + "/*.csv")

list_ = []
print("reading in files")
for file_ in allFiles:
    df = pd.read_csv(file_,index_col=None, header=0)
    list_.append(df)
print("files read in....")

"""
Declares the frame"""
frame = pd.concat(list_, axis = 0, ignore_index = True)

"""
Here we drop all unwanted colums"""
print("dropping unwanted columns")
frame = frame.drop(["Crime ID","Reported by","Location","Latitude","Longitude","LSOA code","LSOA name","Last outcome category","Context"], axis=1)


"""
here we count
"""
print("Counting data")
frame['count_crime'] = frame['Crime type']

#This is oommented out to stop pandas from crashing.
#frame = frame.pivot_table(index=['Month', 'Crime type', 'Falls within'], values='count_crime', aggfunc='count')


"""
Here we map the data
It is done here for efficency reasons
"""
"""
Guys I know this goes against all programming standards but for me to make sure that during every upload that we having the same laout for our dictionary I had to hard code it
Pandas likes to read files in sligyl randomly
"""
constabDic = {'Avon and Somerset Constabulary': 0, 'Devon & Cornwall Police': 1, 'Essex Police': 2, 'South Wales Police': 3, 'Greater Manchester Police': 4, 'Cleveland Police': 5, 'Warwickshire Police': 6, 'British Transport Police': 7, 'North Wales Police': 8, 'Bedfordshire Police': 9, 'Gloucestershire Constabulary': 10, 'West Yorkshire Police': 11, 'Kent Police': 12, 'Merseyside Police': 13, 'Dorset Police': 14, 'Metropolitan Police Service': 15, 'Lincolnshire Police': 16, 'Lancashire Constabulary': 17, 'Leicestershire Police': 18, 'Staffordshire Police': 19, 'West Mercia Police': 20, 'Police Service of Northern Ireland': 21, 'City of London Police': 22, 'Wiltshire Police': 23, 'Northamptonshire Police': 24, 'Derbyshire Constabulary': 25, 'Humberside Police': 26, 'Durham Constabulary': 27, 'South Yorkshire Police': 28, 'Cambridgeshire Constabulary': 29, 'Hertfordshire Constabulary': 30, 'Norfolk Constabulary': 31, 'North Yorkshire Police': 32, 'Surrey Police': 33, 'Hampshire Constabulary': 34, 'Sussex Police': 35, 'Suffolk Constabulary': 36, 'Gwent Police': 37, 'Cumbria Constabulary': 38, 'Northumbria Police': 39, 'Dyfed-Powys Police': 40, 'Thames Valley Police': 41, 'Nottinghamshire Police': 42, 'West Midlands Police': 43, 'Cheshire Constabulary': 44}
frame["Falls within num"] =  frame["Falls within"]
crimeDic = {'Avon and Somerset Constabulary': 0, 'Devon & Cornwall Police': 1, 'Essex Police': 2, 'South Wales Police': 3, 'Greater Manchester Police': 4, 'Cleveland Police': 5, 'Warwickshire Police': 6, 'British Transport Police': 7, 'North Wales Police': 8, 'Bedfordshire Police': 9, 'Gloucestershire Constabulary': 10, 'West Yorkshire Police': 11, 'Kent Police': 12, 'Merseyside Police': 13, 'Dorset Police': 14, 'Metropolitan Police Service': 15, 'Lincolnshire Police': 16, 'Lancashire Constabulary': 17, 'Leicestershire Police': 18, 'Staffordshire Police': 19, 'West Mercia Police': 20, 'Police Service of Northern Ireland': 21, 'City of London Police': 22, 'Wiltshire Police': 23, 'Northamptonshire Police': 24, 'Derbyshire Constabulary': 25, 'Humberside Police': 26, 'Durham Constabulary': 27, 'South Yorkshire Police': 28, 'Cambridgeshire Constabulary': 29, 'Hertfordshire Constabulary': 30, 'Norfolk Constabulary': 31, 'North Yorkshire Police': 32, 'Surrey Police': 33, 'Hampshire Constabulary': 34, 'Sussex Police': 35, 'Suffolk Constabulary': 36, 'Gwent Police': 37, 'Cumbria Constabulary': 38, 'Northumbria Police': 39, 'Dyfed-Powys Police': 40, 'Thames Valley Police': 41, 'Nottinghamshire Police': 42, 'West Midlands Police': 43, 'Cheshire Constabulary': 44}

"""
And this mapps the data """
print("Mapping Constabory data")
print(frame)

"""Prints some infomation that is usefill about the frame
"""
print("***")
#cprint(frame['Falls within'])
print(frame)


"""
Saves to csv
"""
print("saving to CSV...")
frame.to_csv("out.csv")
print("Saved successfully")


"""
Done
"""
print("Done successfully ")
#saved to D:\VAData\2018-09

